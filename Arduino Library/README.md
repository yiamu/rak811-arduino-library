# RAK811 Arduino Library
This Arduino library is compiled based on the RAK811 AT command firmware, so when using this library, the corresponding master control board is required to control the RAK811.

# RAK811 Arduino Library Updated by Zabeel Rahiman on 06/10/2019
Following the recent update of the firmware, the AT Commands have changed for some commands. Those using the RAK811 Arduino Library need to use the updated version of the library uploaded here.
